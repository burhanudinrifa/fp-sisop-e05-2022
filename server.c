#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdbool.h>

int main(){
    int sockfd, ret;
    struct sockaddr_in serverAddr;

    int newSocket;
    struct sockaddr_in newAddr;

    socklen_t addr_size;

    char buffer[1024];
    pid_t childpid;

    bool isSudo = false;
    const char* pass = "admin";
    const char* user = "admin";

    sockfd = socket(AF_INET, SOCK_STREAM,0);
    if(sockfd<0){
        printf("connection error\n");
        return 0;
    }
    printf("server socket successful\n");

    memset(&serverAddr, '\0', sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(8001);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    ret = bind(sockfd, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
    if(ret<0){
        printf("binding error\n");
        return 0;
    }
    printf("bind to port %d\n", 8001);

    if(listen(sockfd, 10) == 0){
        printf("listening\n");
    }else{
        printf("binding error\n");
    }

    while(1){
        newSocket = accept(sockfd, (struct sockaddr*)&newAddr, &addr_size);
        if(newSocket<0){
            return 0;
        }
        printf("connection accepted from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
        if((childpid=fork())==0){
            close(sockfd);
            while(0){
                recv(newSocket, buffer, 1024, 0);
                if(strcmp(buffer, ":exit")==0){
                    printf("connection disconnected from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
                    break;
                }else{
                    printf("Client: %s\n", buffer);
                    send(newSocket, buffer, strlen(buffer), 0);
                    bzero(buffer, sizeof(buffer));
                }
            }
        }
    }

    close(newSocket);

    return 0;
}