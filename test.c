#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

typedef struct rec{
    unsigned int key;
    char f_name[16];
    char l_name[16];
    unsigned int age;
}rec;

int open_rec(char *filename){
    int fd;
    fd = open(filename, O_CREAT | O_RDWR | O_APPEND, 0644);

    if (fd == -1)
        perror("open_record");
    
    return fd;
}

void close_rec(int fd){
    close(fd);
}

int insert_rec(int fd, rec * r){
    int ins;
    ins = write(fd, r, sizeof(rec));

    return ins;
}

int get_rec(int fd, rec *r, int key){
    int gt;
    while ((gt = read(fd, r, sizeof(rec))) != -1)
    {
        if (gt == 0){
            memset(r, 0, sizeof(rec));
            break;
            return gt;
        }

        else if (key == r->key)
        {
            return gt;
        }
        
    }
    memset(r, 0, sizeof(rec));
    return gt;
}

int delete_rec(int fd, int key){
    int del;
    rec *r;
    off_t pos;

    pos = lseek(fd, 0, SEEK_SET);

    while ((del = read(fd, &r, sizeof(rec))) != -1)
    {
        if (del == 0){
            return del;
        }

        else if (key == r->key)
        {
            lseek(fd, 0, SEEK_SET);
            r->key = 0;
            del = write(fd, &r, sizeof(rec));
            return del;
        }
    }
    
}

int main(int argc, char *argv[]){
    int fd;
    rec r;

    fd = open_rec("data1");
    if (argc > 1)
    {
        // insert
        if (argc > 5 && !strcmp(argv[1], "insert"))
        {
            r.key = atoi(argv[2]);
            strcpy(r.f_name, argv[3]);
            strcpy(r.l_name, argv[4]);
            r.age = atoi(argv[5]);
            insert_rec(fd, &r);
        }
        
        // delete
        if (argc > 2 && !strcmp(argv[1], "delete"))
        {
            delete_rec(fd, atoi(argv[2]));
        }
        
        // print
                if (argc > 2 && !strcmp(argv[1], "print"))
        {
            get_rec(fd, &r, atoi(argv[2]));
            printf("key = %d\n", r.key);
            printf("key = %s\n", r.f_name);
            printf("key = %s\n", r.l_name);
            printf("key = %d\n", r.age);
        }
    }
    
    close_rec(fd);
    return 0;
}